==========
!ARCHIVED!
==========

This project has been archived, along with all other POP and Idem-based projects.

* For more details: `Salt Project Blog - POP and Idem Projects Will Soon be Archived <https://saltproject.io/blog/2025-01-24-idem-pop-projects-archived/>`__

==================
idem-data-insights
==================

.. image:: https://img.shields.io/badge/made%20with-pop-teal
   :alt: Made with pop, a Python implementation of Plugin Oriented Programming
   :target: https://pop.readthedocs.io/

.. image:: https://img.shields.io/badge/made%20with-python-yellow
   :alt: Made with Python
   :target: https://www.python.org/

Description
===========

This project is for gathering insights of SLS data.

Run following commands::

    pip install -e .
    idem_data_insights -s [path_to_sls_file]


Run help command to understand more about configuration parameters::

    idem_data_insights --help


More details will be added soon.
